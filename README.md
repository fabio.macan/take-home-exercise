# Task-Home Exercise

This project creates different namespaces for each application, shifts, and users. It helps to divide the cluster's resources and resource quotas. Also, since they have their own database, any access outside its application must be denied. Only the application from within the namespace can access its database.

Each application has its deployment manifest and resources, so they scale independently.

### Structure

There are two separate folders for the manifests resources.
The `admin` folder has the admin resources level, such as namespace creation and RBAC resources. The `developer` folder has the application's resources manifests, and for this exercise, it should impersonate the developer role.

| File                   | Description                                                                 | Path                                                                 |
|------------------------|-----------------------------------------------------------------------------|----------------------------------------------------------------------|
| users-deployment       | Resources for users application                                             | [users-deployment.yaml](developer/users-deployment.yaml)             |
| users-database         | Database resources for users application                                    | [users-database.yaml](developer/users-database.yaml)                 |
| users-database-policy  | Policy to restric access to the database only from users application's pods | [users-database-policy.yaml](developer/users-database-policy.yaml)   |
| shifts-deployment      | Resources for shifts application                                            | [shifts-deployment.yaml](developer/shifts-deployment.yaml)           |
| shifts-database        | Database resources for shifts application                                   | [shifts-database.yaml](developer/shifts-database.yaml)               |
| shifts-database-policy | StatefulSet resources for Users application                                 | [shifts-database-policy.yaml](developer/shifts-database-policy.yaml) |
| developers-rbac        | RBAC controls for developers group                                          | [admin/developers-rbac.yaml](admin/developers-rbac.yaml)             |

- users-deployment.yaml
    - Deployment
    - Service
    - HorizontalPodAutoscaler
    - PodDisruptionBudget

- users-database.yaml
    - Secret
    - Service
    - StatefulSet
    
- users-database-policy.yaml 
    - NetworkPolicy
    
- shifts-deployment.yaml
    - Deployment
    - Service
    - HorizontalPodAutoscaler
    - PodDisruptionBudget

- shifts-database.yaml
    - Secret
    - Service
    - StatefulSet

- shifts-database-policy.yaml
    - NetworkPolicy

- developers-rbac.yaml
    - Namespace
    - ClusterRole
    - ClusterRoleBinding

## Action

The manifests resources are ready for action. Before testing staging and production environments, delete the namespace, some attributes have different values in production compared to staging, and for the database's StatefulSet, some values cannot be changed.

All resources were tested in GKE cluster, although following some instructions for minikube.

### Minikube

If minikube is in action, starting with the following options `--extra-config=kubelet.housekeeping-interval=10s --network-plugin=cni` are important to get the metrics and network plugins working.

Starting minikube.

```sh
minukube start --extra-config=kubelet.housekeeping-interval=10s 
```

Enable metrics.

```sh
minukube addons enable metrics-server
```

In order to make the `NetworkPolicy` working in minikube, a network cni is required, otherwise the following steps can be skipped.

Start the minikube adding another argument `--network-plugin=cni`.

```sh
minukube start --extra-config=kubelet.housekeeping-interval=10s --network-plugin=cni
```

Install [Cilium](https://github.com/cilium/cilium-cli) using its CLI tool. 

```sh
cilium install
```

### Execution

Apply admin manifests.

```sh
kubectl apply -f admin/
```

Apply the manifests impersonating a developer.

```sh
kubectl --as developer --as-group developers apply -f developer/ 
```

To test whether the HPA is working, let's put some pressure on the application.

Create a busy box to send requests for a short period.

```sh
kubectl -n users run makenoise --image=alpine -- sh -c "apk add hey && hey -z 30s -c 20 http://users/get && sleep 31s"
```

Watch the hpa working.

```sh
watch kubectl get hpa -n users 
```

When the stress testing is finished, after 5 minutes, the HPA should scale down the pods.

### RBAC

The RBAC for the developer's group was applied for the cluster, a developer should be able to deploy and roll back their applications, but they can not delete namespaces.

Make changes to the Deployment manifest, adding a label to the template part.

```sh
...

  template:
    metadata:
      labels:
        app: users
        revision: 2
        
...

```

Apply the manifests again using the developer role.

```sh
kubectl --as developer --as-group developers apply -f developer/ 
```

You should be able to see at least two revisions using the `history` command.

```sh
kubectl rollout history deploy users  --as developer --as-group developers -n users
deployment.apps/users 
REVISION  CHANGE-CAUSE
1         <none>
2         <none>
```

Check the labels added to the deployment manifest to ensure the RBAC worked beyond the missing error message.

```sh    
kubectl get deploy -n users -o=jsonpath='{.items[0].spec.template.metadata.labels}'
{"app":"users","revision":"2"}
```

Then still impersonating a developer, undo the last deployment.

```sh
kubectl rollout undo deployment users --as developer --as-group developers -n users
```

Check if the label added now is gone after the rollback.

```sh    
kubectl get deploy -n users -o=jsonpath='{.items[0].spec.template.metadata.labels}'
{"app":"users"}
```

# Bonus

In this section there is example how to deploy the manifests in staging and production environments. Also, using custom metrics to scale applications.

### Multiple Environments deployment.

`Kustomize` is an excellent option for reusing manifests while releasing them in different environments instead of duplicating code.

For this purpose, there are other folders included for this project.

All the manifests previously used were duplicated under the `kustomize` folder as an example of how to use them for multiple environments.

| Folder                                                           | Description                                         |
|------------------------------------------------------------------|-----------------------------------------------------|
| [kustomize/developer/staging](kustomize/developer/staging)       | Resources to be render for stating environment      |
| [kustomize/developer/production](kustomize/developer/production) | Resources to be render for production environment   |
| [kustomize/developer/base](kustomize/developer/base)             | Common resources to be render for both environments |
| [kustomize/admin/staging](kustomize/developer/staging)           | Resources to be render for stating environment      |
| [kustomize/admin/production](kustomize/developer/production)     | Resources to be render for production environment   |
| [kustomize/admin/base](kustomize/developer/base)                 | Common resources to be render for both environments |

Delete the namespaces.

```sh
kubectl delete ns shifts users
```

Apply admin manifests using kustomize.

```sh
kubectl apply -k kustomize/admin/staging
```

Apply the manifests impersonating a developer using kustomize.

```sh
kubectl --as developer --as-group developers apply -k kustomize/developer/staging
```

The same commands can be used for the `production` environment by replacing the `staging` for `production`.

There are two files under the `kustomize/developer/<<environment>>` folder. In production, the [kustomization.yaml](kustomize/developer/production/kustomization.yaml) file has different values compared to the same file in [staging](kustomize/developer/staging/kustomization.yaml).

``` yaml
... 

images:
  - name: mysql
    newName: mysql
    newTag: "5.6"

patches:
  - target:
      kind: HorizontalPodAutoscaler
      name: users
      namespace: users
    patch: |
      - op: replace
        path: /spec/maxReplicas
        value: 7

...
```

Kustomize modifies some values for a specific environment without duplicating all codes.

Another option to set different blocks of codes for each environment, which is found under [kustomize/developer/staging/patch.yaml](kustomize/developer/staging/patch.yaml), compared it to [production file](kustomize/developer/production/patch.yaml), they have different values for resource quotas.

For a better experience, a good idea would be to combine kustomize with a GitOps implementation tool, such as `ArgoCD`, which has a built-in kustomize plugin. All you need to do is to point the project's repo to a folder or a branch, and ArgoCD renders the manifests.

In this example, using `ArgoCD`, the `developer` manifests will be rendered and applied by a `staging` cluster.

```yaml
---
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: users
  namespace: users
  finalizers:
  - resources-finalizer.argocd.argoproj.io
spec:
  destination:
    namespace: users
    server: https://kubernetes.default.svc
  project: apps
  source:
    path: kustomize/developer/staging
    repoURL: https://gitlab.com/fabio.macan/take-home-exercise.git
    targetRevision: main
  syncPolicy:
    automated:
      prune: true
      selfHeal: false
    syncOptions:
    - ApplyOutOfSyncOnly=true
    - CreateNamespace=true
    - PruneLast=true
    retry:
      limit: 0
```

### Custom metrics

For scaling an application using other metrics rather than CPU, `Keda` is an option. Keda allows you to use custom metrics when scaling applications.

I have not scaled applications based on network latency. However, using custom metrics is a way to do it.

Prometheus custom metrics using Keda.

```yaml
---
apiVersion: keda.sh/v1alpha1
kind: ScaledObject
metadata:
 name: users-scale
 namespace: users
spec:
 scaleTargetRef:
   kind: Deployment
   name: users
 minReplicaCount: 1
 maxReplicaCount: 20
 cooldownPeriod: 30
 pollingInterval: 1
 triggers:
   - type: prometheus
     metadata:
       serverAddress: http://prometheus-server
       metricName: users_custom_metrics_keda
       query: |
         sum(avg_over_time(users_connections_request{app="users"}[1m]))
       threshold: "100"
```
